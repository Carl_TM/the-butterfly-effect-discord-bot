# The Butterfly Effect - A Health Reminder Bot for Microhabits in Discord by Carl Kho & Nickie Himaya
# Made for Google DevFest 2020 (Nov. 29, 2020) under the team name It's a feature, not a bug (IaFNaB)
# Made with Python 3.8 & discord.py 1.5.1

# importing the modules
import discord
import discord.ext
from discord.ext import commands
from discord.ext import tasks
from itertools import cycle

# IMPORTANT
# placeChannelID Here
channel_ID = 797520995011002378
# placeBotToken here
botToken = "NzgyMjIxNDcwNTg3NTUxNzY0.X8JCgw.RZmdItcMTHvoR126Ud2iM4_ffJs"

# Set Time Intervals
time_w = 300  # 1 hour
time_q = 30  # 30 minutes
time_l = 100  # 20 minutes

# Enable Commands for discord.py
client = commands.Bot(command_prefix="//")

# (Funny) text in lists for bot reminders
stretchReplies = cycle(
    [
        "@everyone make a **straight back.** Anyare sa likod niyo koya/ate?",
        "Minna-saaaan (@everyone). Ang lahat naman ay naka **proper posture** diba? Naiseu-desu!",
        "Hindi kita gusto, pero slight lang. Bakit ang likod mo, parang boomerang? Balik-balikan mo ako no? Ayieeeee! **STRAIGHTEN YOUR BACK** @everyone",
        "Art thee still sitting upright? @everyone beest sure to **keepeth a straight backeth** all throughout the day!",
        "Are you still sitting straight? @everyone be sure to **keep a straight** back all throughout the day!",
        "Banana ka ba? @everyone dahil **grabe** ang posture mo girl. Straighten up your back naman!",
    ]
)
waterReplies = cycle(
    [
        "You’re not you (@everyone) when you’re thirsty, gulp down a glass of **water!** (Oops, wrong advert. Please don’t sue me, I have a family of larvae!)",
        "Hoy @everyone! Isang oras na. Why don’t you make a trip to the water dispenser at make inom ng **tubig?**",
        "An hour hast hath passed.  Drinketh **watr** r kicketh the (Chum) bucket @everyone",
        "Sana all **nakainom ng tubig.** Ikaw, uminom ka nga ng **tubig** (@everyone). Bot lang ako, ‘di ko yan kaya. :(",
        "One hour has passed. Please make sure to **drink water.** @everyone",
        "The wareduo! One house has passed. I will now resume time to get you (@everyone) to drink water. **Drink, or perish!**",
    ]
)
eyeReplies = cycle(
    [
        "Pag tumingin ka, akin ka. Ayyiiieeee @everyone. Haha **look away from the screen and blink** naman. Dito lang ako (para ‘sayo).",
        "Please **turn away from the screen and voluntarily blink** your eyes, @everyone.",
        "Prithee **turneth hence from the screen and voluntarily blinketh thy eyes** @everyone.",
        "@everyone, please **make tanaw away sa screen** for a while para makagawa ka ng temporary eye rest po.",
    ]
)

# words to have bot DM-remind you
trapWords = [
    "tired",
    "exhausting",
    "break",
    "rest",
    "bye",
    "help",
    "boring",
    "thirsty",
    "sick",
    "hungry",
    "dry",
]

# bot sendMessage code
@client.event
async def on_ready():
    water_reminder.start()
    stretch_reminder.start()
    eye_reminder.start()

    # FirstTime Instructions
    general_channel = client.get_channel(channel_ID)

    waterEmbed = discord.Embed(title="Scheduled Reminders:", color=0x59AEC7)
    waterEmbed.add_field(
        name="#Hydration Nation", value="Hourly water reminders", inline=False
    )
    waterEmbed.add_field(
        name="Posture-awareness & Stretching",
        value="Reminds every 30 minutes",
        inline=False,
    )
    waterEmbed.add_field(
        name="20-20-20 Eye Care",
        value="Every 20 minutes, look at something 20 feet away for 20 seconds",
        inline=False,
    )
    waterEmbed.set_footer(text="Health & Productivity boost in Microhabits")
    waterEmbed.set_author(name="The Butterfly Effect - A Reminder Bot")

    # Intro Message
    await general_channel.send(embed=waterEmbed)
    await general_channel.send("_")
    await general_channel.send("Above are the expected, sample messages.")
    await general_channel.send("_")
    await general_channel.send(
        'Hi! Thank you for inviting ***THE BUTTERFLY EFFECT*** in your server. If you ever need to see this again, please type **"TBE Help"**'
    )


# on users' send message
@client.event
async def on_message(message):
    # if a "trapWord" is detected (indicating low energy)
    if message.content in trapWords:
        await message.author.send("Thought you could escape? Drink water :)")

    # TBE Butterfly first time users / trigger for TBE Help
    if message.content == "TBE Help":
        general_channel = client.get_channel(channel_ID)

        waterEmbed = discord.Embed(title="Scheduled Reminders:", color=0x59AEC7)
        waterEmbed.add_field(
            name="#Hydration Nation", value="Hourly water reminders", inline=False
        )
        waterEmbed.add_field(
            name="Posture-awareness & Stretching",
            value="Reminds every 30 minutes",
            inline=False,
        )
        waterEmbed.add_field(
            name="20-20-20 Eye Care",
            value="Every 20 minutes, look at something 20 feet away for 20 seconds",
            inline=False,
        )
        waterEmbed.set_footer(text="Health & Productivty boost in Microhabits")
        waterEmbed.set_author(name="The Butterfly Effect - A Reminder Bot")

        await general_channel.send(embed=waterEmbed)
        await general_channel.send('If you need to see this again, **type "TBE Help"**')


# water reminder every hour (in seconds)
@tasks.loop(seconds=time_w)
async def stretch_reminder():
    general_channel = client.get_channel(channel_ID)
    await general_channel.send("***Posture/Stretching *** " + next(stretchReplies))


# stretch/posture reminder every 30 minutes (in seconds)
@tasks.loop(seconds=time_q)
async def water_reminder():
    general_channel = client.get_channel(channel_ID)
    await general_channel.send("***Water Reminder *** " + next(waterReplies))


# lookAwayFromScreen reminder every 20 (20-20-20 rule) minutes (in seconds)
@tasks.loop(seconds=time_l)
async def eye_reminder():
    general_channel = client.get_channel(channel_ID)
    await general_channel.send("***20-20-20 rule. *** " + next(eyeReplies))


# Run on server
client.run(botToken)