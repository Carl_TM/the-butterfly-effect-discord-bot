function setup() {
    noCanvas();
    let r = floor(random(3)) + 1;
    let img = createImg('reminders/' + r + '.png');
    img.size(windowWidth, windowHeight);
}
