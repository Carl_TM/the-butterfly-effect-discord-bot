# The Butterfly Effect - A Health Reminder Bot for Microhabits in Discord by Carl Kho & Nickie Himaya
# Made for Google DevFest 2020 (Nov. 29, 2020) under the team name It's a feature, not a bug (IaFNaB)
# Made with Python 3.8 & discord.py 1.5.1

# importing the modules
import discord
import discord.ext
from discord.ext import commands
from discord.ext import tasks
from itertools import cycle

# Enable Commands for discord.py
client = commands.Bot(command_prefix="//")

# IMPORTANT
# placeChannelID Here
channel_ID = 797520995011002378
# placeBotToken here
botToken = "NzgyMjIxNDcwNTg3NTUxNzY0.X8JCgw.RZmdItcMTHvoR126Ud2iM4_ffJs"

# Set Time Intervals
time_w = 10  # 1 hour

funFacts = cycle(
    [
        "You’re not you (@everyone) when you’re thirsty, gulp down a glass of **water!** (Oops, wrong advert. Please don’t sue me, I have a family of larvae!)",
        "Hoy @everyone! Isang oras na. Why don’t you make a trip to the water dispenser at make inom ng **tubig?**",
        "An hour hast hath passed.  Drinketh **watr** r kicketh the (Chum) bucket @everyone",
        "Sana all **nakainom ng tubig.** Ikaw, uminom ka nga ng **tubig** (@everyone). Bot lang ako, ‘di ko yan kaya. :(",
        "One hour has passed. Please make sure to **drink water.** @everyone",
        "The wareduo! One hour has passed. I will now resume time to get @everyone to drink water. **Drink, or perish!**",
    ]
)


# bot sendMessage code
@client.event
async def on_ready():
    print("Bot is on!")

    general_channel = client.get_channel(channel_ID)

    water_reminder.start()


# water reminder every hour (in seconds)
@tasks.loop(seconds=time_w)
async def water_reminder():
    general_channel = client.get_channel(channel_ID)
    await general_channel.send("***Water Reminder *** " + next(funFacts))


# Run on server
client.run(botToken)