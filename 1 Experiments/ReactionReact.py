import discord
from discord.ext import commands

client = commands.Bot(command_prefix="//")
botToken = "NzgyMjIxNDcwNTg3NTUxNzY0.X8JCgw.RZmdItcMTHvoR126Ud2iM4_ffJs"

reaction_title = ""
reactions = {}

@client.command(name="reaction_create_post")
async def reaction_create_post(context):

    embed = discord.Embed(title="Create Reaction Post", color=0x1c1c1c)
    embed.set_author(name="The Butterfly Effect")
    embed.add_field(name="Set Title", value='//reaction_set_title \"New Title\"', inline=False)

    await context.send(embed=embed)
    await context.message.delete()

@client.command(name="reaction_set_title")
async def reaction_set_title(context, new_title):

    global reaction_title
    reaction_title = new_title
    await context.send("The title is now `" + reaction_title + "`!")
    await context.message.delete()

@client.command(name="reaction_add_role")
async def reaction_add_role(context, role, reaction):

    if role != None:
        reactions[role.name] = reaction
        await context.send("Role `" + role.name + "` has been added with the emoji " + reaction + "!")
        await context.message.delete()
    else:
        await context.send("Please try again!")

    print(reactions)
    
@client.event
async def on_ready():
    print("Botbot online!")

client.run(botToken)