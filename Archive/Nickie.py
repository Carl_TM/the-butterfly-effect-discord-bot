# The Butterfly Effect - A Health Reminder Bot for Microhabits in Discord by Carl Kho & Nickie Himaya
# Made for Google DevFest 2020 (Nov. 29, 2020) under the team name It's a feature, not a bug (IaFNaB)
# Made with Python 3.8 & discord.py 1.5.1

# importing the modules
import discord
import time
import discord.ext
from discord.ext import commands
from discord.ext import tasks
from itertools import cycle

# Enable Commands for discord.py
client = commands.Bot(command_prefix="//")

# IMPORTANT
# placeChannelID Here
channel_ID = 782202479290613762
# placeBotToken here
botToken = "NzgyMjIxNDcwNTg3NTUxNzY0.X8JCgw.RZmdItcMTHvoR126Ud2iM4_ffJs"

# Set Time Intervals
time_w = 3600  # 1 hour
time_e = 1200  # 20 minutes


# bot sendMessage code
@client.event
async def on_ready():
    print("Nickie reminder is on!")
    general_channel = client.get_channel(channel_ID)

    nickie_water_reminder.start()
    nickie_eye_reminder.start()


# water reminder every hour (in seconds)
@tasks.loop(seconds=time_w)
async def nickie_water_reminder():
    general_channel = client.get_channel(channel_ID)
    myid = "<@735122369722450001>"
    await general_channel.send(
        "Water reminder! " + "please drink water. I love you, %s!" % myid,
        delete_after=10,
    )


@tasks.loop(seconds=time_e)
async def nickie_eye_reminder():
    general_channel = client.get_channel(channel_ID)
    myid = "<@735122369722450001>"
    await general_channel.send(
        "20-20-20!" + " 20 seconds starts now %s" % myid, delete_after=10
    )
    time.sleep(22)
    await general_channel.send(
        "Okie mana 20 seconds. Adios. %s" % myid, delete_after=10
    )


# Run on server
client.run(botToken)