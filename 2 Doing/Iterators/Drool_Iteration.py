# The Butterfly Effect - A Health Reminder Bot for Microhabits in Discord by Carl Kho & Nickie Himaya
# Made for Google DevFest 2020 (Nov. 29, 2020) under the team name It's a feature, not a bug (IaFNaB)
# Made with Python 3.8 & discord.py 1.5.1

# importing the modules
import discord
import discord.ext
from discord.ext import commands
from discord.ext import tasks
from itertools import cycle

# Enable Commands for discord.py
client = commands.Bot(command_prefix="//")

# IMPORTANT
# placeChannelID Here
channel_ID = 797520995011002378
# placeBotToken here
botToken = "NzgyMjIxNDcwNTg3NTUxNzY0.X8JCgw.RZmdItcMTHvoR126Ud2iM4_ffJs"

# Set Time Intervals
time_w = 4  #1 hour

funFacts = cycle(
    [
        'https://www.youtube.com/watch?v=IehX_6FHTNw',
        'https://www.youtube.com/watch?v=IehX_6FHTNw',
        'https://www.youtube.com/watch?v=SFvLF2W14mw',
        'https://www.youtube.com/watch?v=1ucf8i2-hjc',
        'https://www.youtube.com/watch?v=5yk-ow79w9U',
        'https://www.youtube.com/watch?v=AEhBwiIEdj4',
        'https://www.youtube.com/watch?v=cw2njbV6Kws',
        'https://www.youtube.com/watch?v=WCprtT8Wcr8',
        'https://www.youtube.com/watch?v=3-9JmV8QOkQ',
        'https://www.youtube.com/watch?v=-u8HFzzKX8g',
        'https://www.youtube.com/watch?v=bPQrpm0YcH4',
        'https://www.youtube.com/watch?v=qMnjdV-eFuM',
        'https://www.youtube.com/watch?v=Z2U_cM0_7cA',
        'https://www.youtube.com/watch?v=09smBNps4EM',
        'https://www.youtube.com/watch?v=n98gyETGYTY',
        'https://www.youtube.com/watch?v=TmZmXmaftKA',
        'https://www.youtube.com/watch?v=JtoqR4Omr4M',
        'https://www.youtube.com/watch?v=7iBC0HdSHCI',
        'https://www.youtube.com/watch?v=wN6Svksh7qg',
        'https://www.youtube.com/watch?v=jep1bHKiPb8',
        'https://www.youtube.com/watch?v=QthmpFOrat4',
        'https://www.youtube.com/watch?v=cPoWp_T28eg',
        'https://www.youtube.com/watch?v=fXDVOJaME9g',
        'https://www.youtube.com/watch?v=pTufLoiRnRE',
        'https://www.youtube.com/watch?v=rcw7S7N0iVY',
        'https://www.youtube.com/watch?v=tMSDoDkZN_A',
        'https://www.youtube.com/watch?v=FdOTR2YtiN4',
        'https://www.youtube.com/watch?v=fyrpgfR7Fiw',
        'https://www.youtube.com/watch?v=HBkQf7bKy1g',
        'https://www.youtube.com/watch?v=RXUgk_vSxDk',
        'https://www.youtube.com/watch?v=KqF9gtCv07o',
        'https://www.youtube.com/watch?v=thEkUbfznwQ',
        'https://www.youtube.com/watch?v=jTbttk9yZdo',
        'https://www.youtube.com/watch?v=gsfxGgbZzBg',
        'https://www.youtube.com/watch?v=e3bknT_ZAQc',
        'https://www.youtube.com/watch?v=UGVNqGz0iJg',
        'https://www.youtube.com/watch?v=10l0sXoZLOo',
        'https://www.youtube.com/watch?v=Fm38xK8nXzc',
        'https://www.youtube.com/watch?v=dReWpJWWIjU',
        'https://www.youtube.com/watch?v=v1ozp4knp2M',
        'https://www.youtube.com/watch?v=Vw1xjygOo4c',
        'https://www.youtube.com/watch?v=51MyWRDYBx0',
        'https://www.youtube.com/watch?v=xzF4g0XKWpk',
        'https://www.youtube.com/watch?v=Fw5fBM2osSw',
        'https://www.youtube.com/watch?v=K2vvBZT0xxM',
        'https://www.youtube.com/watch?v=Qp8Tc4ncJgI',
        'https://www.youtube.com/watch?v=AuAYJulMPTI',
        'https://www.youtube.com/watch?v=SmupHGTlznc',
        'https://www.youtube.com/watch?v=JWhc5XbYouY',
        'https://www.youtube.com/watch?v=TDI7XbZ74d4',
        'https://www.youtube.com/watch?v=TDI7XbZ74d4',
        'https://www.youtube.com/watch?v=P_cGM8y8IKM',
        'https://www.youtube.com/watch?v=aGrGYGL3PTw',
        'https://www.youtube.com/watch?v=UZELb_xh7rc',
        'https://www.youtube.com/watch?v=Tw2uxfYLBqI',
        'https://www.youtube.com/watch?v=L1TFnkm1TG8',
        'https://www.youtube.com/watch?v=filrOdIBBpk',
        'https://www.youtube.com/watch?v=cpdEt4bjvkU',
        'https://www.youtube.com/watch?v=Ovqhzil3wJw',
        'https://www.youtube.com/watch?v=ZvUOC-7TGq0',
        'https://www.youtube.com/watch?v=jxfNztGWiZ4',
        'https://www.youtube.com/watch?v=g8hNR71QOtE',
        'https://www.youtube.com/watch?v=qLATPreM2g0',
        'https://www.youtube.com/watch?v=TCa7dzqbSiU',
        'https://www.youtube.com/watch?v=u33o9SJAFlI',
        'https://www.youtube.com/watch?v=n2gj0_y-8tU',
        'https://www.youtube.com/watch?v=FNSr5HtmjKc',
        'https://www.youtube.com/watch?v=D2UCEaRLLo8',
        'https://www.youtube.com/watch?v=B-jqVgQ5JFg',
        'https://www.youtube.com/watch?v=Tjm0mNNvub0',
        'https://www.youtube.com/watch?v=HpSoqzmi4cg',
        'https://www.youtube.com/watch?v=ySB5mOKnTD4',
        'https://www.youtube.com/watch?v=kth9tW6rCiY',
        'https://www.youtube.com/watch?v=eIztihSf2fo',
        'https://www.youtube.com/watch?v=P4b7muX5S_g',
        'https://www.youtube.com/watch?v=l-sfnvDGjWw',
        'https://www.youtube.com/watch?v=qLmiVs1dK-4',
        'https://www.youtube.com/watch?v=laLgahJN3lU',
        'https://www.youtube.com/watch?v=vPgc5A7YR5E',
        'https://www.youtube.com/watch?v=_om-OH3spZc',
        'https://www.youtube.com/watch?v=A2YvFeTQ5qQ',
        'https://www.youtube.com/watch?v=2FVfJTGpXnU',
        'https://www.youtube.com/watch?v=xafug2Fb8_U',
        'https://www.youtube.com/watch?v=OquFw5yTeJM',
        'https://www.youtube.com/watch?v=bcseYHJZAco',
        'https://www.youtube.com/watch?v=RjZxu57CUpI',
        'https://www.youtube.com/watch?v=-xlzF2dqeEM',
        'https://www.youtube.com/watch?v=giTfjD93Ym0',
        'https://www.youtube.com/watch?v=Ilt3cHCH2Ls',
        'https://www.youtube.com/watch?v=1EQVCMPYX-s',
        'https://www.youtube.com/watch?v=4RwuSZDl_do',
        'https://www.youtube.com/watch?v=NFnKaAnXQiY',
        'https://www.youtube.com/watch?v=C_pq2Ps58i8',
        'https://www.youtube.com/watch?v=FOnxKKf66aA',
        'https://www.youtube.com/watch?v=ZNd-F8RwIv8',
        'https://www.youtube.com/watch?v=5GtagVUV3PY',
        'https://www.youtube.com/watch?v=fDdVmnGCXd4',
        'https://www.youtube.com/watch?v=z-g8rBfqNUY',
        'https://www.youtube.com/watch?v=o9x-LdJikLQ',
        'https://www.youtube.com/watch?v=a3792MdX41c',
        'https://www.youtube.com/watch?v=tExkEzS88Po',
        'https://www.youtube.com/watch?v=MKaLiuN9hPo',
        'https://www.youtube.com/watch?v=F9hCmN0AbCY',
        'https://www.youtube.com/watch?v=hCehMnkJt0c',
        'https://www.youtube.com/watch?v=3aaPyvlgjPs',
        'https://www.youtube.com/watch?v=YLLtmzJgjT8',
        'https://www.youtube.com/watch?v=oOJHLXXCL9A',
        'https://www.youtube.com/watch?v=1vAlUvhO_f4',
        'https://www.youtube.com/watch?v=lKiWHxNZxEE',
        'https://www.youtube.com/watch?v=24B31jFS8XU',
        'https://www.youtube.com/watch?v=0lBMQGl4ZXw',
        'https://www.youtube.com/watch?v=sv3TXMSv6Lw',
        'https://www.youtube.com/watch?v=cqxxwtfnKFY',
        'https://www.youtube.com/watch?v=OWlS6uJIp8Y',
        'https://www.youtube.com/watch?v=rrz1m-BImFM',
        'https://www.youtube.com/watch?v=vaPk4-Zgezg',
        'https://www.youtube.com/watch?v=KBdnLCFqFk0',
        'https://www.youtube.com/watch?v=mWHIHCg587s',
        'https://www.youtube.com/watch?v=rO_EDf8ak38',
        'https://www.youtube.com/watch?v=x6O9SwocyKQ',
        'https://www.youtube.com/watch?v=f8emhJLOLXU',
        'https://www.youtube.com/watch?v=InVfF8hp8BA',
        'https://www.youtube.com/watch?v=1jtZ5EjXhJY',
        'https://www.youtube.com/watch?v=La200ONYhvE',
        'https://www.youtube.com/watch?v=oDyH_4QR-jI',
        'https://www.youtube.com/watch?v=0hewRBDEQXY',
        'https://www.youtube.com/watch?v=vIgoAg9821I',
        'https://www.youtube.com/watch?v=vWkf2cuf99U',
        'https://www.youtube.com/watch?v=6l2EisfXZNg',
        'https://www.youtube.com/watch?v=bLRxurBwvQY',
        'https://www.youtube.com/watch?v=icgy1Z25Pnw',
        'https://www.youtube.com/watch?v=GazciozDuYM',
        'https://www.youtube.com/watch?v=rUb5zx6VjJY',
        'https://www.youtube.com/watch?v=rc9TH7tnIjY',
        'https://www.youtube.com/watch?v=MPo2bws8Azc',
        'https://www.youtube.com/watch?v=F-h5SAd9eFM',
        'https://www.youtube.com/watch?v=NhIK2Et5VXg',
        'https://www.youtube.com/watch?v=I5NIWGpil8Q',
        'https://www.youtube.com/watch?v=oofVXopkzrA',
        'https://www.youtube.com/watch?v=NOdlZHGP8Sg',
        'https://www.youtube.com/watch?v=tOZ5weJz57w',
        'https://www.youtube.com/watch?v=la02GXUiWcE',
        'https://www.youtube.com/watch?v=B0XjaSMw72I',
        'https://www.youtube.com/watch?v=O1iogPjKtHg',
        'https://www.youtube.com/watch?v=HcK9jrAy_GU',
        'https://www.youtube.com/watch?v=HcK9jrAy_GU',
        'https://www.youtube.com/watch?v=kecU9qHaWic',
        'https://www.youtube.com/watch?v=kecU9qHaWic',
        'https://www.youtube.com/watch?v=jG4xnTXK-sk',
        'https://www.youtube.com/watch?v=dVPvyLiGXNM',
        'https://www.youtube.com/watch?v=fzi70DKjay4',
        'https://www.youtube.com/watch?v=Lg-4kPiB4NE',
        'https://www.youtube.com/watch?v=lIVdAMJZ8uU',
        'https://www.youtube.com/watch?v=j8I-XZIRZXA',
        'https://www.youtube.com/watch?v=F3hGQWpBN5I',
        'https://www.youtube.com/watch?v=gkbD4j8lFao',
        'https://www.youtube.com/watch?v=KeXfNjbOXFE',
        'https://www.youtube.com/watch?v=RSLwwc_S4Sw',
        'https://www.youtube.com/watch?v=c0cpQefiKmM',
        'https://www.youtube.com/watch?v=kXWCE16INA0',
        'https://www.youtube.com/watch?v=CXfth5AWRns'
    ]
)


# bot sendMessage code
@client.event
async def on_ready():
    print("Bot is on!")

    general_channel = client.get_channel(channel_ID)

    recipeIterator.start()


# HealthyRecipes reminder every hour (in seconds)
@tasks.loop(seconds=time_w)
async def recipeIterator():
    general_channel = client.get_channel(channel_ID)
    await general_channel.send("***You know you're hungry 😋 *** " + next(funFacts))


# Run on server
client.run(botToken)